class BinaryNode():
    def self.__init__(self, item, parent=None,left=None,right=None):
        self.item=item
        self.parent=parent
        self.left=left
        self.right=right
        
def lca_with_parent(root, node1, node2):
    path1 = path_from_root(node1)
    path2 = path_from_root(node2)
    lca = None
    for n1, n2 in zip(path1, path2):
        if n1 != n2:
            return lca
        else:
            lca = n1
    return lca
    
def path_from_root(node):
    path = []
    while node:
        path.insert(0, node)
        node = node.parent
    return path