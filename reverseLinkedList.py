class ListNode():
    def self.__init__(self, item, next=None):
        self.item = item
        self.next = next
    
def reverse_iter(start_node):
    curr = start_node
    prev = None
    next = None
    while curr:
        next = curr.next # 第一件事就是要保存当前节点指向的下一个节点的位置
        curr.next = prev #然后就可以改变当前节点指向的节点了
        prev = curr #然后可以更新prev
        curr = next #以及更新当前节点
    return prev
    
def reverse_recur(start_node):
    if not start_node.next:
        return start_node
    else:
        new_start = reverse_recur(start_node.next) #get to last!
        
    new_start.next.next = new_start #3个节点,把中间.next指向中间.prev
    new_start.next = None #start_node.next设置为None,就是把开头节点.next设为None
    
    return new_start