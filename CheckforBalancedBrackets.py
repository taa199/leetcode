#括号平衡,就是入栈和出栈,碰到左括号就入,右括号就出(出的还是左括号,栈里面只存左括号)
#最后检查stack是不是空的,是空的说明平衡
left_brac = ['(', '[', '{']
right_brac = [')', ']', '}']
right2left = {
    '}' : '{',
    ')' : '(',
    ']' : '['
}
def balance_brac(input):
    stack = []
    for char in input:
        if char in left_brac:
            stack.append(char)
        elif char in right_brac: #如果stack本来就是空的就直接false,否则检查pop出的右括号和左括号是否是一对
            if not stack or stack.pop() != right2left[char]:
                return False
    if stack:
        return False
    return True
