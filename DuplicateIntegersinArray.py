##思路1用一个集合保存已经见过的元素
##思路2用哈系表
##思路3先排序在remove
def remove_duplicates(lst):
    seen_numbers = set()
    i = 0
    while i < len(lst):
        if lst[i] in seen_numbers: #如果见过这个元素
            lst.pop(i)
        else:
            seen_numbers.add(lst[i])
            i += 1
            
## 用这个比较好,因为list长度在改变            
## for (k, v) in enumerate(lst)
