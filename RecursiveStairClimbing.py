def total_ways(n):
    if n <= 2:
        return n
    else:
        return total_ways(n - 1) + total_ways(n - 2)
## 这个时间按复杂度是o(2^n),太耗时
def total_ways2(n):
    helper_arr = [1, 2, 0]
    for i in range(2, n):
        helper_arr[2] = helper_arr[1] + helper_arr[0]
        helper_arr[0] = helper_arr[1]
        helper_arr[1] = helper_arr[2]
    return helper_arr[2]
