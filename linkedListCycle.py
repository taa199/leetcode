# Definition for singly-linked list.
# class ListNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution(object):
    def hasCycle(self, head):
        """
        :type head: ListNode
        :rtype: bool
        """
        hare = head
        tortoise = head
        while hare:
            hare = hare.next
            if not hare:
                return False
            hare = hare.next

            tortoise = tortoise.next
            if hare == tortoise:
                return True
        return False
        
    def countCycle(self, head):
        """
        :type head: ListNode
        :rtype: bool
        """
        hare = head
        tortoise = head
        count = 0
        increment_flag = False
        
        while hare:
            hare = hare.next
            if not hare:
                return False
            hare = hare.next

            tortoise = tortoise.next
            if hare == tortoise:
                if count == 0:
                    increment_flag = True
                else:
                    return count
            if increment_flag:
                count += 1
        return 0        
