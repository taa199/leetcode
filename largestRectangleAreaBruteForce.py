class Solution(object):
    def largestRectangleAreaBruteForce(self, heights):
        """
        :type heights: List[int]
        :rtype: int
        """
        if heights:
            max_rec = max(heights)
        else:
            max_rec = 0
        for idx1, val1 in enumerate(heights):
            for idx2, val2 in enumerate(heights):
                if idx1 >= idx2:
                    continue
                width = idx2 - idx1 + 1
                height = min(heights[idx1:idx2+1])
                current_rec = height * width
                if current_rec > max_rec:
                    max_rec = current_rec
        return max_rec

    def largestRectangleArea(self, heights):
        hstack = []
        pstack = []
        h = 0
        tempH = 0
        tempPos = 0
        tempSize = 0
        maxSize = max(heights)
        for pos in range(0, len(heights)):
            h = heights[pos]
            if len(hstack) == 0 or h > hstack[len(hstack)-1]:
                hstack.append(h)
                pstack.append(pos)
            elif h < hstack[len(hstack)-1]:
                while len(hstack) != 0 and h < hstack[len(hstack)-1]:
                    tempH = hstack.pop()
                    tempPos = pstack.pop()
                    tempSize = tempH * (pos - tempPos)
                    maxSize = max(tempSize, maxSize)
                hstack.append(h)
                pstack.append(tempPos)
        while len(hstack) != 0:
            tempH = hstack.pop()
            tempPos = pstack.pop()
            tempSize = tempH * (pos - tempPos)
            maxSize = max(tempSize, maxSize)
        return maxSize


a = Solution()
print(a.largestRectangleArea([4, 2, 2]))