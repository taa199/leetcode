class BinaryNode():
    def self.__init__(self, item, parent=None,left=None,right=None):
        self.item=item
        self.parent=parent
        self.left=left
        self.right=right
def next_largest(node): 
#找到node的比它大的下一个节点
#分情况找就行了
    #1 如果它有右节点,那么比它大的所有节点都在它右边,返回右子树中最小的.
    if node.right:
        node = node.right
        ##找右子树中最小的,当然要在左边找
        while node.left:
            node = node.left
        return node
    ## 现在已经解决了这个节点有右子树的情况
    
    #2 如果这个节点没有右子树(应该往上找了),也没有父节点,那么没有比它大的了
    if not node.parent: 
        return Node
    #3 没有右子树,有父节点
    while node.parent:
        if node.parent.left = node: ##这个节点本身就是个左节点,直接返回父节点!
            return node.parent
        else:
            node = node.parent ##否则就接着找,找到最顶点,就只能返回None
    return None
        